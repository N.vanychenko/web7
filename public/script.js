//калькулятор из прошлого задания

function onClick() {
  let r = document.getElementById("result");
  r.innerHTML = "Тут должен быть ответ";
  let f1 = document.getElementsByName("field1");
  let f2 = document.getElementsByName("field2");
  if (!(/^[0-9]+$/).test(f1[0].value) || !(/^[0-9]+$/).test(f2[0].value)) {
      window.alert("Please only enter numeric characters!");
      return false;
  }
  r.innerHTML = "Cумма: " + (f1[0].value * f2[0].value) + " руб.";
  if (document.getElementById("defaultCheck1").checked) {
      r.innerHTML = "Cумма: " + ((f1[0].value * f2[0].value) + 100) + " руб.";
  }
  return false;
}

window.addEventListener("DOMContentLoaded", function () {
  let b = document.getElementById("button1");
  b.addEventListener("click", onClick);
  let r = document.getElementById("result");
  r.innerHTML = "Ответ";
});
//можно лисенер для обоих калькуляторов сделать общий

//новый калькулятор
function update() {
  let s = document.getElementsByName("select11");
  let select = s[0]; // 1,2,3
  let price = 0;
  let radio = document.getElementById("radio11");
  let checkbox = document.getElementById("checkbox11");
  //контроль появления и исчезновения доп пунктов (по умолчанию по 1)
  radio.style.display = "none";
  checkbox.style.display = "none";
  //рациональнее проверять радиокнопки и чекбоксы когда они есть на экране
  //поэтому понадобилась конструкция switch
  let radios = document.getElementsByName("options");
  let checkboxes = document.querySelectorAll("#checkbox11 input");
  //эти штуки здесь только потому что jslint не нравится объявление в switch
  let i = 0;
  switch (select.value) {
  case "1":
      price = 100;
      break;
  case "2":
      price = 200;
      radio.style.display = "block";
      //console.log(radios.value);
      for (i = 0; i < radios.length; i += 1) {
          if (radios[i].checked == 1) {
              price += parseInt(radios[i].value);
              break;
          }
      }
      break;
  case "3":
      price = 50;
      checkbox.style.display = "block";
      checkboxes.forEach(function (checkbox) {
      //console.log(checkbox.value);
          if (checkbox.checked) {
              price += parseInt(checkbox.value);
          }
      });
      break; //зачем?
  }
  let textbox = document.getElementsByName("field11");
  textbox[0].value = textbox[0].value.replace(/[^0-9]/g, "");
  //не даем возможность ввести ничего кроме цифр (безысходность)

  let r = document.getElementById("price");
  //console.log(price);
  //console.log(textbox[0].value);
  r.innerHTML = "Cумма: " + ((textbox[0].value) * (price)) + " руб.";
}



window.addEventListener("DOMContentLoaded", function () {
//новый код
  $('.gallery').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    //adaptiveHeight: true, //только при одном слайде
    responsive: [
      {
        breakpoint: 720, //мобильные
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          //adaptiveHeight: true
        }
      },
    ]
  });
//
  document.getElementById("field11").onkeyup = update;
  //textbox.addEventListener("keypress", update);
  //по умолчанию надо скрыть радиокнопки и чекбоксы (1 выбор)
  let radio = document.getElementById("radio11");
  radio.style.display = "none";
  let checkbox = document.getElementById("checkbox11");
  checkbox.style.display = "none";
  //обработчик изменения select
  let s = document.getElementsByName("select11");
  let select = s[0];
  select.addEventListener("change", function () {
      update();
  });
  //обработка изменения радиокнопок
  let radiooptions = document.getElementsByName("options");
  radiooptions.forEach(function (radio) {
      radio.addEventListener("change", function () {
          update();
      });
  });
  //обработка изменения чекбоксов
  let checkboxes = document.getElementsByName("check");
  checkboxes.forEach(function (checkbox) {
      checkbox.addEventListener("change", function () {
          update();
      });
  });
  update();
});